package com.cn.liukaku.service.admintw.test.service;


import com.cn.liukaku.service.admintw.ServiceAdminApplication;
import com.cn.liukaku.service.admintw.domain.LiukakuUser;
import com.cn.liukaku.service.admintw.service.AdminService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Date;

//@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServiceAdminApplication.class)
//@ActiveProfiles(value = "prod")
public class AdminServiceTest {


    @Resource
    private AdminService adminService;

    @Test
    void register() {
        LiukakuUser liukakuUser = new LiukakuUser();
        liukakuUser.setId(123);
        liukakuUser.setName("andre");
        liukakuUser.setInfo("info -andre");
        liukakuUser.setCreateDay(new Date());
        adminService.register(liukakuUser);
    }

    @Test
    void login() {
        LiukakuUser liukakuUser = adminService.login(1, "123123");
        Assert.assertNotNull(liukakuUser);
    }
}
