package com.cn.liukaku.service.admintw.service;

import com.cn.liukaku.service.admintw.domain.LiukakuUser;


public interface AdminService {

    /**
     * 登录
     * @param liukakuUser 用户对象
     */
    public void register(LiukakuUser liukakuUser);

    public LiukakuUser login(int id ,String password);
}
