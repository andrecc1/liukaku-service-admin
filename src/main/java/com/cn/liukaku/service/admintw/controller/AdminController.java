package com.cn.liukaku.service.admintw.controller;

import com.cn.liukaku.common.dto.BaseResult;
import com.cn.liukaku.service.admintw.domain.LiukakuUser;
import com.cn.liukaku.service.admintw.service.AdminService;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class AdminController {

    @Resource
    private AdminService adminService;

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public BaseResult login(String loginCode, String password) {

        System.out.println("##################");

        int id=Integer.parseInt(loginCode);
        BaseResult baseResult = checkLogin(id, password);

        if (baseResult != null) {
            return baseResult;
        }

        LiukakuUser liukakuUser = adminService.login(id, password);

        if (liukakuUser != null) {
            return BaseResult.ok(liukakuUser);
        } else {
            return BaseResult.notOk(Lists.newArrayList(new BaseResult.Error("", "登录失败")));
        }
    }

    private BaseResult checkLogin(int id, String password) {
        BaseResult baseResult = null;
        List<BaseResult.Error> errors = Lists.newArrayList();

        if (id == 0 || StringUtils.isBlank(password)) {
            baseResult = BaseResult.notOk(Lists.newArrayList(
                    new BaseResult.Error("loginid", "登录账户不能为空"),
                    new BaseResult.Error("loginid", "登录账户不能为空")
            ));
        }

        return baseResult;
    }
}
