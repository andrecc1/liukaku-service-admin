package com.cn.liukaku.service.admintw.mapper;

import com.cn.liukaku.service.admintw.domain.Luser;
import java.util.List;

public interface LuserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Luser record);

    Luser selectByPrimaryKey(Integer id);

    List<Luser> selectAll();

    int updateByPrimaryKey(Luser record);
}