package com.cn.liukaku.service.admintw.mapper;

import com.cn.liukaku.service.admintw.domain.LiukakuUser;
import java.util.List;

public interface LiukakuUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LiukakuUser record);

    LiukakuUser selectByPrimaryKey(Integer id);

    List<LiukakuUser> selectAll();

    int updateByPrimaryKey(LiukakuUser record);
}