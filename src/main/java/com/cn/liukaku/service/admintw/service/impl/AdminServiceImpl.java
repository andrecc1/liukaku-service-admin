package com.cn.liukaku.service.admintw.service.impl;

import com.cn.liukaku.service.admintw.domain.LiukakuUser;
import com.cn.liukaku.service.admintw.mapper.LiukakuUserMapper;
import com.cn.liukaku.service.admintw.service.AdminService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional(readOnly = true)
public class AdminServiceImpl implements AdminService {

    @Resource
    private LiukakuUserMapper liukakuUserMapper;

    @Override
    @Transactional(readOnly = false)
    public void register(LiukakuUser liukakuUser) {
        liukakuUserMapper.insert(liukakuUser);
    }

    @Override
    public LiukakuUser login(int id, String password) {
        return liukakuUserMapper.selectByPrimaryKey(id);
    }
}
