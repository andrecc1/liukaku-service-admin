package com.cn.liukaku.service.admintw.mapper;

import com.cn.liukaku.service.admintw.domain.Generatortest;
import java.util.List;

public interface GeneratortestMapper {
    int insert(Generatortest record);

    List<Generatortest> selectAll();
}